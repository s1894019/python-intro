# Introduction to Python
Introduction to Python course aimed at complete beginners.

The purpose of this course is to introduce you to the fundamentals of computer programming and to build your confidence in self-study. For the duration of the course we will be covering general programming concepts and structures which are not exclusive to Python and exist in other programming languages as well.

The course is based on the wonderful Jupyter Notebooks which you can install from [here](http://jupyter.org/install). Alternatively, if you are from the University of Edinburgh you can access the programming environment using [Noteable](https://noteable.edina.ac.uk/) which can be accessed through the accompanying learn course *Introdcution to Python*. You can self-enrol in the course at take it at your own pace via the self-enrol tab on [Learn](https://learn.ed.ac.uk).

### Setting up in Noteable
If you are using Noteable, then the easiest way to get the necessary files in the course is by running the following command in a notebook **within Noteable**.
```
!git clone https://git.ecdf.ed.ac.uk/digital_skills/python-intro.git
```

## Structure

### Part 0
Short intro to the Python programming language and Jupyter notebooks.

### Part 1
Basic programming concepts - variables, types, operations and user interaction.

### Part 2
Higher level data structures, if statements, loops, functions and classes.

### Further Exercises
A compilation of additional exercises covering the previous material.

Part 1 and 2 come with exercises in the end.

It is estimated that the notebooks should take a user an average of 4 hours to complete.
